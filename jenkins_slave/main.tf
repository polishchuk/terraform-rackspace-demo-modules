resource "openstack_compute_keypair_v2" "ssh_key" {
  name = "ssh_key"
  region = "${var.region}"
  public_key = "${file("~/.ssh/id_rsa.pub")}"
}

resource "openstack_compute_instance_v2" "jenkins_slave" {
  name = "${var.instance_name}-${var.environment_name}-${count.index}"
  region = "${var.region}"
  image_name = "${var.image_name}"
  flavor_id = "${var.flavor_id}"
  count = "${var.count}"

  key_pair = "ssh_key"

  # network {
  #   uuid = "00000000-0000-0000-0000-000000000000"
  #   name = "public"
  # }

  # network {
  #   uuid = "11111111-1111-1111-1111-111111111111"
  #   name = "private"
  # }
  provisioner "chef"  {
        # attributes_json = <<-EOF
        # {
        #     "key": "value",
        #     "app": {
        #         "cluster1": {
        #             "nodes": [
        #                 "webserver1",
        #                 "webserver2"
        #             ]
        #         }
        #     }
        # }
        # EOF
        environment = "integration"
        run_list = ["role[jenkins-slave]"]
        node_name = "jenkins-slave-tf-${var.environment_name}-${count.index}"
        secret_key = "${file("~/.chef/encrypted_data_bag_secret")}"
        server_url = "https://manage.chef.io/organizations/thomascook"
        recreate_client = true
        user_name = "iuriipolishchuk"
        user_key = "${file("~/.chef/iuriipolishchuk.pem")}"
        version = "11.16.2"
        ssl_verify_mode = "verify_peer"
        connection {
          type = "ssh"
          user = "root"
          private_key = "${file("~/.ssh/id_rsa")}"
          agent = false
        }
    }
}
