output "instance_private_ip" {
  value = ["${openstack_compute_instance_v2.jenkins_slave.*.network.0.fixed_ip_v4}"]
}