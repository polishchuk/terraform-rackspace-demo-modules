variable "region" {
  description = "RackSpace region"
  default = "LON"
}

variable "image_name" {
  description = "Image name"
  default = "CentOS 7 (PVHVM)"
}

variable "flavor_id" {
  description = "Flavor ID"
  default = "general1-4"
}

variable "instance_name" {
  description = "Instance name (type), for example: jenkins-slave"
  default = "jenkins-slave"
}

variable "environment_name" {
  description = "Environment name, for example: staging"
  default = "integration"
}

variable "count" {
	description = "Instance count"
	default = 2
}
